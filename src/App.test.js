import { render, screen, cleanup } from '@testing-library/react';
import App from './App';

afterEach(cleanup)
describe('test App', () =>{
  it('should take a snapshot', () => {
    const { asFragment } = render(<App />)
    
    expect(asFragment(<App />)).toMatchSnapshot()
   })
})

