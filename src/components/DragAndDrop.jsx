import React from 'react'
import {useState} from 'react'
import './DragAndDrop.css'

const items = [
 { number: "1", title: "Task 1"},
 { number: "2", title: "Task 2"},
 { number: "3", title: "Task 3"},
 { number: "4", title: "Task 4"},
 { number: "5", title: "Task 5"},
]

const initialDnDState = {
 draggedFrom: null,
 draggedTo: null,
 isDragging: false,
 originalOrder: [],
 updatedOrder: []
}

const DragToReorderList = () => {
 
 const [list, setList] = React.useState(items);
 const [dragAndDrop, setDragAndDrop] = React.useState(initialDnDState);
 
 
 const onDragStart = (event) => {
  const initialPosition = Number(event.currentTarget.dataset.position);
  
  setDragAndDrop({
   ...dragAndDrop,
   draggedFrom: initialPosition,
   isDragging: true,
   originalOrder: list
  });
  
  
   event.dataTransfer.setData("text/html", '');
 }
 
 const onDragOver = (event) => {
  
  event.preventDefault();
  
  let newList = dragAndDrop.originalOrder;
 
  const draggedFrom = dragAndDrop.draggedFrom; 

  const draggedTo = Number(event.currentTarget.dataset.position); 

  const itemDragged = newList[draggedFrom];
  const remainingItems = newList.filter((item, index) => index !== draggedFrom);

   newList = [
    ...remainingItems.slice(0, draggedTo),
    itemDragged,
    ...remainingItems.slice(draggedTo)
   ];
    
  if (draggedTo !== dragAndDrop.draggedTo){
   setDragAndDrop({
    ...dragAndDrop,
    updatedOrder: newList,
    draggedTo: draggedTo
   })
  }

 }
 
 const onDrop = (event) => {
  
  setList(dragAndDrop.updatedOrder);
  
  setDragAndDrop({
   ...dragAndDrop,
   draggedFrom: null,
   draggedTo: null,
   isDragging: false
  });
 }
 
 	return(
		<section className="drag-drop">
   <ul>
    
    {list.map( (item, index) => {
     return(
      <li 
       key={index}
       
       data-position={index}
       draggable
       
       onDragStart={onDragStart}
       onDragOver={onDragOver}
       onDrop={onDrop}
       
       className="dropArea"
       >
        <p>{item.title}</p>
        
      </li>
     )
    })}
     
   </ul>
		</section>
		)
};

export default DragToReorderList