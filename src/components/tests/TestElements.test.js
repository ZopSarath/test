import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import TestElements from '../TestElements'

afterEach(cleanup)

describe('test TestElements', () => {
    it('should be equal to 0', () => {
    const { getByTestId } = render(<TestElements />); 
    expect(getByTestId('counter')).toHaveTextContent(0)   
    })
    it('increments counter', () => {
        const { getByTestId } = render(<TestElements />); 
        
        fireEvent.click(getByTestId('increment'))
    
        expect(getByTestId('counter')).toHaveTextContent('1')
      })
    
      it('decrements counter', () => {
        const { getByTestId } = render(<TestElements />); 
        
        fireEvent.click(getByTestId('decrement'))
    
        expect(getByTestId('counter')).toHaveTextContent('-1')
      })
})