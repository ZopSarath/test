import React, { useState, useEffect } from 'react'
import { QueryClient, QueryClientProvider} from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import DragToReorderList from './DragAndDrop'
import useViewportWidth from "./useViewportWidth"
import QueryExample from './Query'


const queryClient = new QueryClient()

const TestElements = () => {
    const initialValue = 0
    const [count,setCount] = useState(initialValue)
    const [display, setDisplay] = useState({})
    const viewportWidth = useViewportWidth()

    const dataFetch = new Promise((resolve, reject) => {
        resolve({id: 1, inside: {type: 'Ds', difficulty: 'easy'}})
    }) 

    const fetchData = async () => { await dataFetch.then((res) => setDisplay(res))} 

    useEffect(()=>{
        fetchData()
    }, [])

    
    return(
        <>
            <QueryClientProvider client={queryClient}>
                <QueryExample />
                <ReactQueryDevtools initialIsOpen={false} />
                <hr/>
            <section data-testid="counter">Count: {count}</section>
            <button data-testid="increment" onClick={()=>setCount(count+1)}>Increment</button>
            <button data-testid="decrement" onClick={()=>setCount(count-1)}>Decrement</button>
            <section>Display: {display.id}</section>
            <DragToReorderList/>
            <p>The viewport width is: {viewportWidth}</p>
            </QueryClientProvider>
            
        </>
    )
    
}

export default TestElements